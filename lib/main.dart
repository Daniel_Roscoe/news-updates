import 'package:flutter/material.dart';
import 'package:news_updates/viewmodels/newsArticlesListViewModel.dart';
import 'package:provider/provider.dart';
import 'pages/newsListPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "COVID-19 Updates",
        debugShowCheckedModeBanner: false,
        home: ChangeNotifierProvider(
            builder: (_) => NewsArticleListViewModel(), child: NewsListPage()));
  }
}
